﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dijkstra
{
    class Program
    {
        static void Main(string[] args)
        {
            List<VertList> Vs = LoadData();
            PrintVertList("Starting State",Vs);

            // 1] Start at source vertex X (1 in our case)
            SetExplored(Vs, 1, true);
            SetShortestPath(Vs, 1, 0);



            // Main loop we will grow X by one node... which node to pick


            for (int i = 1; i < Vs.Count; i++)
            {
                // in my list of unexplored nodes, which edges are external?
                // for each node in X (nodes visited so far) what are the immeadiate outgoing edges that are connected to vertices that have not been viewed?

                Edge ee = GetNextEdge(Vs);
                SetExplored(Vs, ee.To, true);
                SetShortestPath(Vs, ee.To, ee.Weight);

            }

            PrintShortestPaths("Final Result", Vs);

            Console.ReadLine();

        }

        private static void SetShortestPath(List<VertList> vs, int vertex, int sp)
        {
            VertList vl = vs.Find(x => x.VertexFrom == vertex);
            vl.ShortestPath = sp;
        }

        private static Edge GetNextEdge( List<VertList> AllVerts)
        {
            List<Edge> el = new List<Edge>();
            foreach (var item in AllVerts)
            {
                if (item.Explored == true)
                {
                    foreach (var ed in item.EdgeList)
                    {
                        if (ed.ToExplored == false)
                        {

                            int shp = GetVertexShortestPath(AllVerts, ed.From);
                            Edge eTest = new Edge(ed.From, ed.To, shp + ed.Weight);

                            el.Add(eTest);
                        }
                    }
                }
            }


            el.Sort((x, y) => x.Weight.CompareTo(y.Weight) );

         //   PrintEdgeList("Next Edge",el);

            Edge E = el[0];
            Console.WriteLine(E);
            return E;
        }


        private static int GetVertexShortestPath(List<VertList> Vs, int Vertex)
        {

            VertList vl = Vs.Find(x => x.VertexFrom == Vertex);
            int sp = vl.ShortestPath;

            return sp;

        }

        private static void PrintVertList(string msg , List<VertList> V)
        {
            Console.WriteLine(msg);
            foreach (var item in V)
            {
                Console.WriteLine(item);
            }
        }


        private static void PrintShortestPaths(string msg, List<VertList> V)
        {
            Console.WriteLine(msg);
            foreach (var item in V)
            {
                Console.WriteLine("Vertex {0}, Shortest Path {1}",item.VertexFrom,item.ShortestPath );
            }
        }


        private static void PrintEdgeList(string msg, List<Edge> Es)
        {
            Console.WriteLine(msg);
            foreach (var item in Es)
            {
                Console.WriteLine(item);
            }
        }

        private static List<VertList> LoadData()
        {
            string[] lines = File.ReadAllLines(@"Z:\develop\CourseRA\Algorithms-1\Week5\DijkstraData.txt");
      //     string[] lines = File.ReadAllLines(@"Z:\develop\CourseRA\Algorithms-1\Week5\test1.txt");
            List<VertList> v = new List<VertList>();

            foreach (var item in lines)
            {
                string[] data = item.Split(new Char[] { '\t' });
                int numItems = data.Length;
                int vertexFrom = int.Parse(data[0]);
                List<Edge> listEdge = new List<Edge>();
                
                for (int k = 1; k < numItems - 1; k++)
                {
                    string[] pair = data[k].Split(new Char[] { ',' });
                    int vertextTo = int.Parse(pair[0]);
                    int weight = int.Parse(pair[1]);
                    Edge e = new Edge(vertexFrom, vertextTo, weight);
                    listEdge.Add(e);
                }
                v.Add(new VertList(vertexFrom, listEdge));
            }
            return v;
        }



        private static void SetExplored(List<VertList> Vs , int vertex, bool setvalue) {
            foreach (var vert in Vs)
            {
                if (vert.VertexFrom == vertex) vert.Explored = setvalue;

                foreach (var ed in vert.EdgeList)
                {
                    if (ed.From == vertex) ed.FromExplored = setvalue;
                    if (ed.To == vertex) ed.ToExplored = setvalue;

                }
            }
        }

    }

    class VertList
    {
        public int VertexFrom { get; }
        public int ShortestPath { get; set; }
        public List<Edge> EdgeList { get; }
        public string EdgeString { get; }
        public bool Explored { get; set; }

        public VertList( int vertexFrom , List<Edge> edgeList)
        {
            VertexFrom = vertexFrom;
            EdgeList = edgeList;
            Explored = false;
            ShortestPath = 2000000000; 
        }

        private string List2String(List<Edge> e)
        {
            string s = "";
            foreach (var item in e)
            {
                s = s + item.To + "("+ item.ToExplored +")(" + item.Weight + ") ";
            }
            return s;
        }

        public override string ToString() => $"Vertex: {this.VertexFrom}({this.Explored}) OutEdges: {List2String(EdgeList)} ";
    }

    class Edge
    {
        public int From { get; }
        public bool FromExplored { get; set; }
        public int To { get; }
        public bool ToExplored { get; set; }
        public int Weight { get; }

        public Edge(int from, int to, int weight) { From = from;  To = to;  Weight = weight; FromExplored = false; ToExplored = false; }
        public override string ToString() => $"From: {this.From}({this.FromExplored}) To: {this.To}({this.ToExplored})({this.Weight}) ";
    }
}


